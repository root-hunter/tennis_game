package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlayerTest {
	
	@Test
	public void scoreShouldBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Act
		player.incrementScore();
		
		
		//Assert
		assertEquals(1, player.getScore());
	}
	
	
	@Test
	public void scoreShouldNotBeIncreased() {
		//Arrange
		Player player = new Player("Federer", 0);
				
		//Assert
		assertEquals(0, player.getScore());
	}
	
	
	@Test
	public void scoreShouldBeLove() {
		//Arrange
		Player player = new Player("Federer", 0);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertEquals("love", translation);
	}
	
	@Test
	public void scoreShouldBeFifteen() {
		//Arrange
		Player player = new Player("Federer", 1);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertEquals("fifteen", translation);
	}
	
	@Test
	public void scoreShouldBeThirty() {
		//Arrange
		Player player = new Player("Federer", 2);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertEquals("thirty", translation);
	}
	
	@Test
	public void scoreShouldBeForty() {
		//Arrange
		Player player = new Player("Federer", 3);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertEquals("forty", translation);
	}
	
	@Test
	public void scoreShouldBeNullIfNegative() {
		//Arrange
		Player player = new Player("Federer", -1);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertNull(translation);
	}
	
	@Test
	public void scoreShouldBeNullIfTooBig() {
		//Arrange
		Player player = new Player("Federer", 4);
		
		//Act
		String translation = player.getScoreAsString();
		
		//Assert
		assertNull(translation);
	}
	
	@Test
	public void scoreShouldBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 3);

		//Act
		boolean result = player1.isTieWith(player2);
		
		
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void scoreShouldNotBeTie() {
		//Arrange
		Player player1 = new Player("Federer", 1);
		Player player2 = new Player("Nadal", 3);

		//Act
		boolean result = player1.isTieWith(player2);
		
		
		//Assert
		assertFalse(result);
	}
	
	@Test
	public void scoreShouldBeLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);

		//Act
		boolean result = player1.hasAtLeastFortyPoints();
		
		
		//Assert
		assertTrue(result);
	}
	
	
	@Test
	public void scoreShouldBeNotLeastFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);

		//Act
		boolean result = player1.hasAtLeastFortyPoints();
		
		
		//Assert
		assertFalse(result);
	}
	
	
	@Test
	public void scoreShouldBeLessFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 2);

		//Act
		boolean result = player1.hasLessThanFortyPoints();
		
		
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void scoreShouldBeNotLessFortyPoints() {
		//Arrange
		Player player1 = new Player("Federer", 3);

		//Act
		boolean result = player1.hasLessThanFortyPoints();
		
		
		//Assert
		assertFalse(result);
	}
	
	
	@Test
	public void scoreShouldMoreThanFourty() {
		//Arrange
		Player player1 = new Player("Federer", 4);

		//Act
		boolean result = player1.hasMoreThanFourtyPoints();
		
		
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void scoreShouldNotMoreThanFourty() {
		//Arrange
		Player player1 = new Player("Federer", 3);

		//Act
		boolean result = player1.hasMoreThanFourtyPoints();
		
		
		//Assert
		assertFalse(result);
	}
	
	@Test
	public void playerHaveOnePointOfAdvantage() {
		//Arrange
		Player player1 = new Player("Federer", 2);
		Player player2 = new Player("Nadal", 1);

		//Act
		boolean result = player1.hasOnePointAdvantageOn(player2);
		
		
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void playerNotHaveOnePointOfAdvantage() {
		//Arrange
		Player player1 = new Player("Federer", 1);
		Player player2 = new Player("Nadal", 3);

		//Act
		boolean result = player1.hasOnePointAdvantageOn(player2);
		
		
		//Assert
		assertFalse(result);
	}
	
	@Test
	public void playerHaverLeastTwoPointOfAdvatage() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 0);

		//Act
		boolean result = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		
		
		//Assert
		assertTrue(result);
	}
	
	@Test
	public void playerNotHaverLeastTwoPointOfAdvatage() {
		//Arrange
		Player player1 = new Player("Federer", 3);
		Player player2 = new Player("Nadal", 2);

		//Act
		boolean result = player1.hasAtLeastTwoPointsAdvantageOn(player2);
		
		
		//Assert
		assertFalse(result);
	}
}
