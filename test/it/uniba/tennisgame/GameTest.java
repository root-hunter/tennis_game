package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The Game class represents a tennis game. This class should be used as
 * follows:
 * 
 * // Create a new game: 
 * Game game = new Game("name1", "name2");
 *
 * // Get the name of the two players: 
 * String playerName1 = game.getPlayerName1(); 
 * String playerName2 = game.getPlayerName2();
 *
 * // Update the game by incrementing the score of either the first or second
 * player: 
 * game.incrementPlayerScore(playerName1);
 * game.incrementPlayerScore(playerName2);
 *
 * // Get the status of the game: 
 * String status = game.getGameStatus();
 * 
 * See the README.md file to understand the rules behind the game status returned by tennis_game
 * 
 */

public class GameTest {
	
	

	@Test
	public void testFifteenThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act	
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void testFourtyThirty() throws DuplicatedPlayerException, GameHasAlreadyBeWonException {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		
		//Act	
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);

		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		
		String status = game.getGameStatus();
		
		//Assert
		assertEquals("Federer forty - Nadal thirty", status);
	}

}
